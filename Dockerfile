FROM rust AS build

# create empty shell project
RUN USER=root cargo new --bin canary-demo-client

WORKDIR /canary-demo-client
COPY Cargo.lock Cargo.toml ./

RUN cargo build --release 
RUN find . -iname 'canary[-_]demo[-_]client*' | xargs rm -rf && rm -rf src

COPY src/ src/

RUN cargo build --release

# ------------------------------

FROM debian:8-slim

WORKDIR /usr/canary-demo-client/
COPY --from=build /canary-demo-client/target/release/canary-demo-client .

ENTRYPOINT ["/usr/canary-demo-client/canary-demo-client"]