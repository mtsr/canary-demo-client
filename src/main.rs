#![deny(warnings)]
extern crate clap;
extern crate env_logger;
extern crate futures;
extern crate hyper;
extern crate tokio;
extern crate tokio_signal;
extern crate tokio_timer;
#[macro_use]
extern crate log;

use clap::{App, Arg};
use futures::stream;
use hyper::rt::{self, Future, Stream};
use hyper::Client;
use std::time::Duration;
use tokio_signal::unix::{Signal, SIGINT, SIGTERM};

fn main() -> Result<(), ()> {
    let matches = App::new("canary-demo-client")
        .version("0.1")
        .about("...")
        .author("Jonas Matser")
        .arg(
            Arg::with_name("url")
                .short("u")
                .long("url")
                .help("Sets the url to call")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("concurrency")
                .short("c")
                .long("concurrency")
                .help("Sets number of concurrent connections to make")
                .default_value("8")
                .validator(|val| {
                    usize::from_str_radix(&val, 10)
                        .map(|_| ())
                        .map_err(|_| "Not a valid number".to_owned())
                }),
        )
        .arg(
            Arg::with_name("rate")
                .short("r")
                .long("rate")
                .help("Sets maximum number of connections per second")
                .default_value("10000")
                .validator(|val| {
                    u32::from_str_radix(&val, 10)
                        .map(|_| ())
                        .map_err(|_| "Not a valid number".to_owned())
                }),
        )
        .arg(
            Arg::with_name("log_rps")
                .short("l")
                .long("logrps")
                .help("Log average requests per second"),
        )
        .get_matches();

    let url = matches.value_of("url").unwrap();

    // panicfree unwraps through default value and validator
    let concurrency = usize::from_str_radix(matches.value_of("concurrency").unwrap(), 10).unwrap();
    let rate = u32::from_str_radix(matches.value_of("rate").unwrap(), 10).unwrap();
    let log_rps = matches.is_present("log_rps");

    let env = env_logger::Env::default().filter_or(env_logger::DEFAULT_FILTER_ENV, "info");
    env_logger::Builder::from_env(env).init();

    let client = Client::new();

    let count = std::sync::Arc::new(std::sync::atomic::AtomicUsize::new(0));
    let start = std::time::Instant::now();

    info!(
        "Querying {} at {} requests per second and {} concurrent requests",
        url, rate, concurrency
    );

    let url = url.parse::<hyper::Uri>().unwrap();
    if url.scheme_part().map(|s| s.as_ref()) != Some("http") {
        error!("This example only works with 'http' URLs.");
        return Err(());
    }

    let urls = stream::repeat::<hyper::Uri, ()>(url);

    let interval = tokio_timer::Interval::new_interval(Duration::new(0, 1_000_000_000 / rate))
        .map_err(|_| error!("Error"))
        .zip(urls);

    let count_logger = count.clone();

    let work = interval
        .map(move |(_, url)| {
            let count = count.clone();

            client
                .get(url)
                .map_err(|err| {
                    error!("Error: {}", err);
                })
                .and_then(|response| {
                    debug!("{:?}", response);
                    Ok(())
                })
                .and_then(move |_| {
                    count.fetch_add(1, std::sync::atomic::Ordering::SeqCst);
                    Ok(())
                })
        })
        .buffer_unordered(concurrency)
        .for_each(move |_| Ok(()));

    // Create a stream for each of the signals we'd like to handle.
    let sigint = Signal::new(SIGINT).flatten_stream();
    let sigterm = Signal::new(SIGTERM).flatten_stream();

    // Use the `select` combinator to merge these two streams into one
    let stream = sigint.select(sigterm);

    let shutdown = stream.take(1).into_future().map_err(|_e| error!("Error"));

    if log_rps && log_enabled!(log::Level::Info) {
        let counter = tokio_timer::Interval::new_interval(Duration::new(5, 0))
            .map(move |_| -> Result<(), ()> {
                let count = count_logger.load(std::sync::atomic::Ordering::Relaxed);
                info!(
                    "{} requests in {} seconds ~= {} rps",
                    count,
                    (std::time::Instant::now() - start).as_secs(),
                    count as f32 / (std::time::Instant::now() - start).as_secs() as f32
                );
                Ok(())
            })
            .map_err(|_| error!("Error"))
            .for_each(move |_| Ok(()));
        rt::run(
            work.select2(shutdown)
                .select2(counter)
                .map(|_| ())
                .map_err(|_| ()),
        );
    } else {
        rt::run(work.select2(shutdown).map(|_| ()).map_err(|_| ()));
    }

    Ok(())
}
